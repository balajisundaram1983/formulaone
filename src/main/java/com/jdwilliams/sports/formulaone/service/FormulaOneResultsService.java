package com.jdwilliams.sports.formulaone.service;

import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.repository.FormulaOneResultsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormulaOneResultsService {

    private FormulaOneResultsRepository resultsRepository;

    @Autowired
    public FormulaOneResultsService(FormulaOneResultsRepository resultsRepository){
        this.resultsRepository = resultsRepository;
    }

    public ResultsModel getResults(int driverResultSize, int teamResultSize){
        return resultsRepository.getResults(driverResultSize, teamResultSize);
    }
}
