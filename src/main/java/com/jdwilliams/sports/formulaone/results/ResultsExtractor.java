package com.jdwilliams.sports.formulaone.results;

import com.jdwilliams.sports.formulaone.exception.DocumentNotFoundException;
import com.jdwilliams.sports.formulaone.model.DriverResultModel;
import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.model.TeamResultModel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ResultsExtractor {

    @Value("${f1-results.url}")
    private String resultUrl;

    @Value("${f1-results.url-timeout-millis}")
    private int timeOutMillis;

    private static final String TROW = "tr";
    private static final String TBODY = "tbody";
    private static final String POSITION_CSS_CLASS = "msr_pos";
    private static final String TEAM_NAME_CSS_CLASS = "msr_team";
    private static final String DRIVER_NAME_CSS_CLASS = "msr_driver";
    private static final String CSS_TOTAL_POINTS_CLASS = "msr_total";
    private static final String TEAM_RESULTS_CSS_CLASS = "motor-sport-results msr_season_team_results";
    private static final String DRIVER_RESULTS_CSS_CLASS = "motor-sport-results msr_season_driver_results";

    public ResultsModel getResults(int driverResultSize, int teamResultSize){
        try {
            Document document = getHtmlDocument(resultUrl, timeOutMillis);
            ResultsModel results = new ResultsModel();
            results.setDriverResults(getDriverResults(document, driverResultSize));
            results.setTeamResults(getTeamResults(document, teamResultSize));
            return results;
        } catch (IOException e) {
            throw new DocumentNotFoundException("Document could not be produced", e);
        }
    }

    private List<TeamResultModel> getTeamResults(Document document, int teamResultSize){
        Elements teamResultsElement = document.getElementsByClass(TEAM_RESULTS_CSS_CLASS);
        return teamResultsElement
                .select(TBODY).select(TROW)
                    .stream()
                    .filter(this::filterPredicate)
                    .limit(teamResultSize)
                    .map(this::constructTeamResult)
                    .collect(Collectors.toList());
    }

    public Document getHtmlDocument(String url, int timeOutMillis) throws IOException{
        return Jsoup.parse(new URL(url), timeOutMillis);
    }

    private List<DriverResultModel> getDriverResults(Document document, int driverResultSize){
        Elements driverResultsElement = document.getElementsByClass(DRIVER_RESULTS_CSS_CLASS);
        return driverResultsElement.select(TBODY).select(TROW)
                .stream()
                .filter(this::filterPredicate)
                .limit(driverResultSize)
                .map(this::constructDriverResult)
                .collect(Collectors.toList());
    }

    private DriverResultModel constructDriverResult(Element driverRow){
        DriverResultModel driverResultModel = new DriverResultModel();
        driverResultModel.setName(driverRow.getElementsByClass(DRIVER_NAME_CSS_CLASS).text().trim());
        driverResultModel.setPosition(Integer.parseInt(driverRow.getElementsByClass(POSITION_CSS_CLASS).text().trim()));
        driverResultModel.setPoints(Long.parseLong(driverRow.getElementsByClass(CSS_TOTAL_POINTS_CLASS).text().trim()));
        return driverResultModel;
    }

    private TeamResultModel constructTeamResult(Element teamRow){
        TeamResultModel teamResultModel = new TeamResultModel();
        teamResultModel.setName(teamRow.getElementsByClass(TEAM_NAME_CSS_CLASS).text().trim());
        teamResultModel.setPosition(Integer.parseInt(teamRow.getElementsByClass(POSITION_CSS_CLASS).text().trim()));
        teamResultModel.setPoints(Long.parseLong(teamRow.getElementsByClass(CSS_TOTAL_POINTS_CLASS).text().trim()));
        return teamResultModel;
    }

    private boolean filterPredicate(Element row){
        return !row.getElementsByClass(POSITION_CSS_CLASS).text().isEmpty();
    }
}