package com.jdwilliams.sports.formulaone.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TeamResultModel {

    private String name;
    private long points;
    private int position;
}
