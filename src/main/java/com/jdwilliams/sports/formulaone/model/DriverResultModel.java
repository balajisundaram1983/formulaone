package com.jdwilliams.sports.formulaone.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DriverResultModel {

    private String name;
    private long points;
    private int position;

}
