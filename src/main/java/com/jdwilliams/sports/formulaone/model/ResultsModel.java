package com.jdwilliams.sports.formulaone.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ResultsModel {

    private List<DriverResultModel> driverResults;
    private List<TeamResultModel> teamResults;
}
