package com.jdwilliams.sports.formulaone.exception;

public class DocumentNotFoundException extends RuntimeException{

    public DocumentNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
