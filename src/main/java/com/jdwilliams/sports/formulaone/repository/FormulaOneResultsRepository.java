package com.jdwilliams.sports.formulaone.repository;

import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.results.ResultsExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class FormulaOneResultsRepository {

    private ResultsExtractor resultsExtractor;

    @Autowired
    public FormulaOneResultsRepository(ResultsExtractor resultsExtractor){
        this.resultsExtractor = resultsExtractor;
    }

    public ResultsModel getResults(int driverResultSize, int teamResultSize){
        return resultsExtractor.getResults(driverResultSize, teamResultSize);
    }

}
