package com.jdwilliams.sports.formulaone.controller;

import com.jdwilliams.sports.formulaone.exception.DocumentNotFoundException;
import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.service.FormulaOneResultsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class FormulaOneResultsController {

    private FormulaOneResultsService resultsService;
    private Logger logger = LoggerFactory.getLogger(FormulaOneResultsController.class);

    @Autowired
    public FormulaOneResultsController(FormulaOneResultsService resultsService){
        this.resultsService = resultsService;
    }

    @GetMapping("/formulaone/results/2018")
    public ResultsModel getTeamResults(@RequestParam(value="driverResultsLimit", defaultValue = "10") int driverResultsLimit, @RequestParam(value="teamResultsLimit", defaultValue = "10") int teamResultsLimit){
        return resultsService.getResults(driverResultsLimit, teamResultsLimit);
    }

    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(DocumentNotFoundException.class)
    public void handleDocumentNotFoundExceptions(DocumentNotFoundException e){
        logger.error("DocumentNotFoundException occurred: ", e);
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public void handleExceptions(Exception e){
        logger.error("Exception occurred: ", e);
    }

}