const formulaOneResultsModule = (() => {
    const FORMULA_ONE_RESULTS_URL = "formulaone/results/2018";
    const DRIVER_RESULTS_BODY = document.getElementById('driver-results-body');
    const TEAM_RESULTS_BODY = document.getElementById('team-results-body');

    const loadResults = () => {
         fetch(`${FORMULA_ONE_RESULTS_URL}?driverResultsLimit=10&teamResultsLimit=5`)
           .then(
             function(response) {
               if (response.status !== 200) {
                 console.log(`Error Status Code: ${response.status}`);
                 return;
               }
               response.json().then(function(data) {
                 renderResults(JSON.stringify(data));
                 initialiseDataTable();
               });
             }
           )
           .catch(function(err) {
             console.log('Fetch Error:', err);
           });
    }

    const renderResults = (results) => {
        const resultsObj = JSON.parse(results);
        renderResultsTable(resultsObj.driverResults, DRIVER_RESULTS_BODY);
        renderResultsTable(resultsObj.teamResults, TEAM_RESULTS_BODY);
    }

    const renderResultsTable = (results, tableBody) => {
        results.forEach(function(result) {
            const row = document.createElement("tr");
            createCells(row, [result["position"],result["name"],result["points"]]);
            tableBody.appendChild(row);
        });
    }

    const createCells = (row, cellTexts) => {
        cellTexts.forEach(function(cellText) {
            const cell = document.createElement("td");
            cell.setAttribute("class","content");
            const cellTextNode = document.createTextNode(cellText);
            cell.appendChild(cellTextNode);
            row.appendChild(cell);
        });
    }

    const initialiseDataTable = () => {
        $('#driver-results').DataTable({
            "bFilter" : false,
            "bJQueryUI" : true,
            "bPaginate": false,
            "bInfo": false
        });
        $('#team-results').DataTable({
             "bFilter" : false,
             "bJQueryUI" : true,
             "bPaginate": false,
             "bInfo": false
        });
    }

    return {
        loadResults: loadResults
    }

})();

formulaOneResultsModule.loadResults();