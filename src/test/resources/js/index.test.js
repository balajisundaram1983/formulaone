require('jsdom-global')();
global.fetch = require("node-fetch");
const assert = require("chai").assert;
const fetchMock = require("fetch-mock");
const formulaOneResultsModule = require("../../../main/resources/static/js/index");

fetchMock.restore().getOnce('http://localhost:8080/formulaone/results/2018?driverResultsLimit=10&teamResultsLimit=5', {});