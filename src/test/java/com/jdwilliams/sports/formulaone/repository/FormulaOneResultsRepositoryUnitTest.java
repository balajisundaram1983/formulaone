package com.jdwilliams.sports.formulaone.repository;

import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.results.ResultsExtractor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FormulaOneResultsRepositoryUnitTest {

    @Mock
    private ResultsExtractor mockResultsExtractor;
    @Mock
    private ResultsModel expectedResults;
    private FormulaOneResultsRepository repository;

    @Before
    public void setUp(){
        repository = new FormulaOneResultsRepository(mockResultsExtractor);
    }

    @Test
    public void should_return_same_results_as_given_by_results_extractor(){
        when(mockResultsExtractor.getResults(5,5)).thenReturn(expectedResults);
        ResultsModel resultsModel = repository.getResults(5, 5);
        assertTrue(resultsModel == expectedResults);
    }

    @Test
    public void should_pass_same_parameters_to_results_extractor(){
        when(mockResultsExtractor.getResults(5,5)).thenReturn(expectedResults);
        repository.getResults(5, 5);
        verify(mockResultsExtractor,times(1)).getResults(5,5);
    }

}
