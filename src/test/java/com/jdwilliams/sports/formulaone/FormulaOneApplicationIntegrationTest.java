package com.jdwilliams.sports.formulaone;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdwilliams.sports.formulaone.model.ResultsModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FormulaOneApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(port=0)
@ActiveProfiles("test")
public class FormulaOneApplicationIntegrationTest {

	private String formulaOneResultsHtml;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void should_return_correct_results_for_2018_formulaone_championship() throws Exception {

		formulaOneResultsHtml = new String(Files.readAllBytes(Paths.get(ClassLoader.getSystemResource("formulaone_results.html").toURI())));

		stubFor(get(urlEqualTo("/f1-results/2018-f1-championship-standings/"))
						.willReturn(aResponse()
						.withStatus(200)
						.withHeader("Content-Type", "text/html")
						.withBody(formulaOneResultsHtml)));

		ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://localhost:" + port + "/formulaone/results/2018", String.class);
		ResultsModel resultsModel = new ObjectMapper().readValue(responseEntity.getBody(), ResultsModel.class);
		assertThat(resultsModel.getDriverResults().size(),is(1));
		assertThat(resultsModel.getTeamResults().size(),is(1));

		assertThat(resultsModel.getDriverResults().get(0).getName(),is("Lewis Hamilton"));
		assertThat(resultsModel.getDriverResults().get(0).getPoints(),is(400L));
		assertThat(resultsModel.getDriverResults().get(0).getPosition(),is(1));

		assertThat(resultsModel.getTeamResults().get(0).getName(),is("Mercedes"));
		assertThat(resultsModel.getTeamResults().get(0).getPoints(),is(700L));
		assertThat(resultsModel.getTeamResults().get(0).getPosition(),is(1));

	}

	@Test
	public void should_return_service_unavailable_when_formulaone_results_uri_down(){
		stubFor(get(urlEqualTo("/f1-results/2018-f1-championship-standings/"))
				.willReturn(aResponse()
						.withStatus(404)));
		ResponseEntity<String> responseEntity = restTemplate.getForEntity("http://localhost:" + port + "/formulaone/results/2018", String.class);
		assertThat(responseEntity.getStatusCode(),is(HttpStatus.SERVICE_UNAVAILABLE));
	}

}