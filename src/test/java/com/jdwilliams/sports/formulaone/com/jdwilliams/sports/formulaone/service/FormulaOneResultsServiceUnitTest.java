package com.jdwilliams.sports.formulaone.com.jdwilliams.sports.formulaone.service;

import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.repository.FormulaOneResultsRepository;
import com.jdwilliams.sports.formulaone.service.FormulaOneResultsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FormulaOneResultsService.class})
public class FormulaOneResultsServiceUnitTest {

    @Mock
    private FormulaOneResultsRepository resultsMockRepo;
    private FormulaOneResultsService resultsService;
    private ResultsModel expectedResultsModel;

    @Before
    public void setUp(){
        resultsService = new FormulaOneResultsService(resultsMockRepo);
        expectedResultsModel = new ResultsModel();
    }

    @Test
    public void should_return_same_results_as_given_by_repository(){
        when(resultsMockRepo.getResults(5,5)).thenReturn(expectedResultsModel);
        ResultsModel resultsModelReturned = resultsService.getResults(5,5);
        assertTrue(resultsModelReturned == expectedResultsModel);
    }

    @Test
    public void should_call_repository_with_correct_expected_results_size(){
        resultsService.getResults(1,1);
        verify(resultsMockRepo, times(1)).getResults(1,1);
    }

}