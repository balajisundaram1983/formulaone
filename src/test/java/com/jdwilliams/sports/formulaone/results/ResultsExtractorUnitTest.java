package com.jdwilliams.sports.formulaone.results;

import com.jdwilliams.sports.formulaone.model.DriverResultModel;
import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.model.TeamResultModel;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Jsoup.class, ResultsExtractor.class})
public class ResultsExtractorUnitTest {

    @Mock
    private Document expectedDocument;
    @Mock
    private Element rowElementMock;
    @Mock
    private Elements mockElements;

    private String resultUrl = "http://test-url/results";

    private ResultsExtractor resultsExtractor = new ResultsExtractor();
    private static final String POSITION_CSS_CLASS = "msr_pos";
    private static final String DRIVER_NAME_CSS_CLASS = "msr_driver";
    private static final String TEAM_NAME_CSS_CLASS = "msr_team";
    private static final String CSS_TOTAL_POINTS_CLASS = "msr_total";

    @Before
    public void setUp(){
        PowerMockito.mockStatic(Jsoup.class);
    }

    @Test
    public void should_return_a_document_produced_by_Jsoup() throws IOException {
        when(Jsoup.parse(any(URL.class),anyInt())).thenReturn(expectedDocument);
        Document htmlDocument = resultsExtractor.getHtmlDocument(resultUrl, 3000);
        assertTrue(htmlDocument == expectedDocument);
    }

    @Test(expected = MalformedURLException.class)
    public void should_throw_Malformed_Exception_When_url_is_invalid() throws Exception{
        resultsExtractor.getHtmlDocument(null, 3000);
    }

    @Test(expected = IOException.class)
    public void should_throw_IOException_When_Jsoup_cannot_connect_to_url() throws Exception{
        when(Jsoup.parse(any(URL.class),anyInt())).thenThrow(new IOException("Test IOException occurred"));
        resultsExtractor.getHtmlDocument(resultUrl, 3000);
    }

    @Test
    public void should_return_false_when_position_css_class_text_is_empty() throws Exception{
        Elements elements = new Elements();
        when(rowElementMock.getElementsByClass(POSITION_CSS_CLASS)).thenReturn(elements);
        assertFalse(Whitebox.invokeMethod(resultsExtractor,"filterPredicate", rowElementMock));
    }

    @Test
    public void should_return_true_when_position_css_class_text_is_not_empty() throws Exception{
        Element positionElement = constructElement("td","text is not empty");
        Elements elements = new Elements(Arrays.asList(positionElement));
        when(rowElementMock.getElementsByClass(POSITION_CSS_CLASS)).thenReturn(elements);
        assertTrue(Whitebox.invokeMethod(resultsExtractor,"filterPredicate", rowElementMock));
    }

    @Test
    public void should_construct_correct_driver_Result_from_driver_row() throws Exception{
        Element positionElement = constructElement("td","1");
        Element pointsElement = constructElement("td","400");
        Element nameElement = constructElement("td","Lewis Hamilton");

        when(rowElementMock.getElementsByClass(POSITION_CSS_CLASS)).thenReturn(new Elements(Arrays.asList(positionElement)));
        when(rowElementMock.getElementsByClass(DRIVER_NAME_CSS_CLASS)).thenReturn(new Elements(Arrays.asList(nameElement)));
        when(rowElementMock.getElementsByClass(CSS_TOTAL_POINTS_CLASS)).thenReturn(new Elements(Arrays.asList(pointsElement)));

        DriverResultModel driverResultModel = Whitebox.invokeMethod(resultsExtractor, "constructDriverResult", rowElementMock);
        assertThat(driverResultModel.getName(),is("Lewis Hamilton"));
        assertThat(driverResultModel.getPoints(),is(400L));
        assertThat(driverResultModel.getPosition(),is(1));
    }

    @Test
    public void should_construct_correct_team_Result_from_team_row() throws Exception{
        Element positionElement = constructElement("td","1");
        Element pointsElement = constructElement("td","700");
        Element nameElement = constructElement("td","Ferrari");

        when(rowElementMock.getElementsByClass(POSITION_CSS_CLASS)).thenReturn(new Elements(Arrays.asList(positionElement)));
        when(rowElementMock.getElementsByClass(TEAM_NAME_CSS_CLASS)).thenReturn(new Elements(Arrays.asList(nameElement)));
        when(rowElementMock.getElementsByClass(CSS_TOTAL_POINTS_CLASS)).thenReturn(new Elements(Arrays.asList(pointsElement)));

        TeamResultModel teamResultModel = Whitebox.invokeMethod(resultsExtractor, "constructTeamResult", rowElementMock);
        assertThat(teamResultModel.getName(),is("Ferrari"));
        assertThat(teamResultModel.getPoints(),is(700L));
        assertThat(teamResultModel.getPosition(),is(1));
    }

    @Test
    public void should_limit_driver_results_correctly() throws Exception{
        Document document = new Document("");
        document.html("<html>" +
                            "<body>" +
                            "<table>" +
                                "<tbody class=\"motor-sport-results msr_season_driver_results\">" +
                                    "<tr><td class=\"msr_pos\">1</td><td class=\"msr_driver\">Lewis Hamilton</td><td class=\"msr_total\">400</td></tr>" +
                                    "<tr><td class=\"msr_pos\">2</td><td class=\"msr_driver\">Jenson Button</td><td class=\"msr_total\">300</td></tr>" +
                                    "<tr><td class=\"msr_pos\">3</td><td class=\"msr_driver\">Weber</td><td class=\"msr_total\">200</td></tr>" +
                                "</tbody>" +
                            "</table>" +
                            "</body>" +
                        "</html>");
        List<DriverResultModel> results = Whitebox.invokeMethod(resultsExtractor, "getDriverResults", document, 1);
        assertThat(results.size(),is(1));
        assertThat(results.get(0).getName(), is("Lewis Hamilton"));
        assertThat(results.get(0).getPosition(), is(1));
        assertThat(results.get(0).getPoints(), is(400L));
    }

    @Test
    public void should_return_correct_size_team_results() throws Exception{
        Document document = new Document("");
        document.html("<html>" +
                        "<body>" +
                            "<table>" +
                                "<tbody class=\"motor-sport-results msr_season_team_results\">" +
                                    "<tr><td class=\"msr_pos\">1</td><td class=\"msr_team\">Mercedes</td><td class=\"msr_total\">700</td></tr>" +
                                    "<tr><td class=\"msr_pos\">2</td><td class=\"msr_team\">Ferrari</td><td class=\"msr_total\">400</td></tr>" +
                                    "<tr><td class=\"msr_pos\">3</td><td class=\"msr_team\">Renault</td><td class=\"msr_total\">300</td></tr>" +
                                "</tbody>" +
                            "</table>" +
                        "</body>" +
                    "</html>");
        List<TeamResultModel> results = Whitebox.invokeMethod(resultsExtractor, "getTeamResults", document, 1);
        assertThat(results.size(),is(1));
        assertThat(results.get(0).getName(), is("Mercedes"));
        assertThat(results.get(0).getPosition(), is(1));
        assertThat(results.get(0).getPoints(), is(700L));
    }

    @Test
    public void should_return_correct_team_and_driver_results() throws Exception {
        DriverResultModel driverResultModel1 = new DriverResultModel("Lewis Hamilton",700,1);
        DriverResultModel driverResultModel2 = new DriverResultModel("Jensen Button",600,2);
        TeamResultModel teamResultModel1 = new TeamResultModel("Mercedes",900,1);
        TeamResultModel teamResultModel2 = new TeamResultModel("Ferrari",800,2);

        ResultsExtractor spyResultExtractor = spy(resultsExtractor);

        doReturn(expectedDocument).when(spyResultExtractor).getHtmlDocument(anyString(),anyInt());
        doReturn(Arrays.asList(driverResultModel1,driverResultModel2)).when(spyResultExtractor,"getDriverResults", any(Document.class), anyInt());
        doReturn(Arrays.asList(teamResultModel1,teamResultModel2)).when(spyResultExtractor,"getTeamResults", any(Document.class), anyInt());

        ResultsModel results = spyResultExtractor.getResults(1, 1);
        assertTrue(results.getDriverResults().size() == 2);
        assertTrue(results.getTeamResults().size() == 2);
    }

    private Element constructElement(String elementTagName, String elementText){
        Element element = new Element(elementTagName);
        element.appendText(elementText);
        return element;
    }


}
