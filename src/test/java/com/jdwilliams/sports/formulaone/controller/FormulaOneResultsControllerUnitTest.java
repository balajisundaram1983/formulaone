package com.jdwilliams.sports.formulaone.controller;

import com.jdwilliams.sports.formulaone.exception.DocumentNotFoundException;
import com.jdwilliams.sports.formulaone.model.ResultsModel;
import com.jdwilliams.sports.formulaone.service.FormulaOneResultsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({FormulaOneResultsController.class})
public class FormulaOneResultsControllerUnitTest {

    @Mock
    private FormulaOneResultsService mockService;
    @Mock
    private Logger logger;

    private FormulaOneResultsController controller;
    private ResultsModel expectedResultsModel;

    @Before
    public void setUp(){
        controller = new FormulaOneResultsController(mockService);
        expectedResultsModel = new ResultsModel();
        Whitebox.setInternalState(controller,"logger", logger);
    }

    @Test
    public void should_return_same_results_as_given_by_service(){
        when(mockService.getResults(5,5)).thenReturn(expectedResultsModel);
        ResultsModel resultsModel = controller.getTeamResults(5, 5);
        assertTrue(resultsModel == expectedResultsModel);
    }

    @Test
    public void should_pass_correct_size_parameters_to_service(){
        when(mockService.getResults(5,5)).thenReturn(expectedResultsModel);
        controller.getTeamResults(5, 5);
        verify(mockService, times(1)).getResults(5,5);
    }

    @Test
    public void should_log_error_when_exception_thrown(){
        RuntimeException exception = new RuntimeException("Test exception");
        controller.handleExceptions(exception);
        verify(logger, times(1)).error("Exception occurred: ", exception);
    }

    @Test
    public void should_log_error_when_Document_Not_Found(){
        DocumentNotFoundException exception = new DocumentNotFoundException("Test exception", new RuntimeException("Document not found"));
        controller.handleDocumentNotFoundExceptions(exception);
        verify(logger, times(1)).error("DocumentNotFoundException occurred: ", exception);
    }

}
