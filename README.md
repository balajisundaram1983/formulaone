# FormulaOne 2018 Championship Standings Application

### Run Instructions

#### Prerequisite

Following software's need to be installed on your local machine

- Java 8 and Gradle(v5.1) or Docker for local machine

#### Port used

Application uses port 8080 to start. 
Please make 8080 available for this application.

### Run Command

__Using Docker:__

From root of the project please run the following commands 

docker build -f Dockerfile -t formulaone .

docker run -it -p 8080:8080 formulaone

__Using Gradle Wrapper:__

From root of the project, please run the following command

./gradlew clean build (Cleans, runs test cases and builds a jar)

./gradlew bootrun (Runs springboot)

__Using Gradle:__

From root of the project, please run the following command

gradle clean build bootrun

once this is done, please visit [http://localhost:8080/] in your browser.
The results might take time to load based on the internet speed 
and based on the response time from https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/ 

#### Technologies/Libraries used

- SpringBoot
- Jquery
- Docker
- Gradle
- BootStrap css
- Jquery Datatable

#### My local environment
I have used following tools/softwares in my local for this development

- Gradle v5.1
- Docker for Mac -v17.09.1-ce-mac42
- Intellij IDE - v2018.3

#### Notes
Please don't hesitate to contact me on email __balaji.sundaram1983@gmail.com__
if you have any troubles starting this application. 