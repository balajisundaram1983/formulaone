FROM java:8
EXPOSE 8080

ADD /build/libs/formulaone-1.0.jar formulaone.jar
ENTRYPOINT ["java","-jar","formulaone.jar"]